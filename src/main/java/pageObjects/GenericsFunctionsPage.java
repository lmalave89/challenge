package pageObjects;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class GenericsFunctionsPage {

	WebDriver driver;

	public GenericsFunctionsPage(WebDriver driver) {
		this.driver = driver;
	}

	public void checkElementVisibleByElement(WebElement element) {
		try {
			WebDriverWait wait = new WebDriverWait(driver, 10);
			wait.until(ExpectedConditions.visibilityOf(element));
		} catch (Exception e) {
			System.out.println("Not found this element " + element);
		}
	}

	public void checkElementClickeableByElement(WebElement element) {
		try {
			WebDriverWait wait = new WebDriverWait(driver, 10);
			wait.until(ExpectedConditions.elementToBeClickable(element));
		} catch (Exception e) {
			System.out.println("Not found this element " + element);
		}
	}	

	public void clickElement(WebElement element){
		this.checkElementClickeableByElement(element);
		element.click();
	}

	public void sendDataToElement(WebElement element, String value) {
		this.checkElementVisibleByElement(element);
		element.sendKeys(value);
	}
	
	public void pressDown(WebElement element) {
		element.sendKeys(Keys.DOWN);
	}	
	
	public void pressEnter(WebElement element) {
		element.sendKeys(Keys.ENTER);
	}		

}
