package pageObjects;
 
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

//import DataFeatures.DataUserGmail;
import managers.FileReaderManager;

public class GooglePage {
	
	WebDriver driver;
	FileReaderManager fileReaderManager; 
	
	public GooglePage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}
	
	
	@FindBy(how = How.NAME, using = "q") 
	public  WebElement inputSearch;
	
	@FindBy(how = How.NAME, using = "btnK") 
	public WebElement buttonSearch;
	
	@FindBy(how = How.ID, using = "hdtb-msb") 
	public WebElement menu;	
	
	@FindBy(how = How.PARTIAL_LINK_TEXT, using = "The Books - Patrick Rothfuss") 
	public WebElement link;		
	
	@FindBy(how = How.CLASS_NAME, using = "yellowborder") 
	public WebElement page;	
	
	@FindBy(how = How.CLASS_NAME, using = "erkvQe") 
	public WebElement list;		
	
	@FindBy(how = How.CLASS_NAME, using = "sbct") 
	public WebElement firtsSuggestion;		
	
	
	
	
	public void navigateToGooglePage() {
		driver.get(FileReaderManager.getInstance().getConfigReader().getApplicationUrl());
	}	
}
	