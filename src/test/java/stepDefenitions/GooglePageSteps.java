package stepDefenitions;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import injectionDependenciesManager.TestContext;
import pageObjects.GooglePage;


public class GooglePageSteps{

	TestContext testContext;
	GooglePage googlePage;	
	private String search = "The name of the wind";
	private String searchIncomplete = "The name of the w";
	
	public GooglePageSteps(TestContext context) {
		testContext = context;
		googlePage = testContext.getPageObjectManager().getGooglePage();
		googlePage.navigateToGooglePage();
	}

	@Given("^I’m on the homepage$")
	public void imOnTheHomepage() throws InterruptedException{
		googlePage.navigateToGooglePage();
	}
	
	@When("^I type “The name of the wind” into the search field$")
	public void i_type_The_name_of_the_wind_into_the_search_field() throws InterruptedException{
		testContext.getGenericsFunctionsPage().sendDataToElement(googlePage.inputSearch, this.search);
	}	

	@When("^I click the Google Search button$")
	public void i_click_the_Google_Search_button(){
		testContext.getGenericsFunctionsPage().clickElement(googlePage.buttonSearch);
	}

	@Then("^I go to the search results page$")
	public void i_go_to_the_search_results_page(){
		testContext.getGenericsFunctionsPage().checkElementVisibleByElement(googlePage.menu);
	}

	@Then("^the first result is “The Name of the Wind - Patrick Rothfuss”$")
	public void the_first_result_is_The_Name_of_the_Wind_Patrick_Rothfuss(){
		testContext.getGenericsFunctionsPage().checkElementVisibleByElement(googlePage.link);
	}

	@When("^I click on the first result link$")
	public void i_click_on_the_first_result_link(){
		testContext.getGenericsFunctionsPage().clickElement(googlePage.link);
	}

	@Then("^I go to the “Patrick Rothfuss - The Books” page$")
	public void i_go_to_the_Patrick_Rothfuss_The_Books_page(){
		testContext.getGenericsFunctionsPage().checkElementVisibleByElement(googlePage.page);
	}

	@When("^I type “The name of the w” into the search field$")
	public void i_type_The_name_of_the_w_into_the_search_field(){
		testContext.getGenericsFunctionsPage().sendDataToElement(googlePage.inputSearch, this.searchIncomplete);
	}

	@When("^the suggestions list is displayed$")
	public void the_suggestions_list_is_displayed(){
		testContext.getGenericsFunctionsPage().checkElementVisibleByElement(googlePage.list);
	}

	@When("^I click on the first suggestion in the list$")
	public void i_click_on_the_first_suggestion_in_the_list(){
		testContext.getGenericsFunctionsPage().clickElement(googlePage.firtsSuggestion);
	}
	
}
